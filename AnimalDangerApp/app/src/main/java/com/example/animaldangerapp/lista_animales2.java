package com.example.animaldangerapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class lista_animales2 extends AppCompatActivity {
    private Button Siguiente;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_animales2);
        Siguiente = (Button) findViewById(R.id.btnsiguiente);
        Siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(lista_animales2.this, lista_animales3.class);
                startActivity(intent);
            }
        });
    }
}